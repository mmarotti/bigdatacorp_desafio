# -*- coding: utf-8 -*-

from unicodedata import normalize


# Retira espaços que podem vir a existir na resposta
def retira_espacos (S):
    if (S[0] == " "):
        S = S[1:]
    if (S[len(S)-1] == " "):
        S = S[:-1]
    return S

def retira_uppercase_acentos (S):
    S = S.decode("utf-8")
    S = normalize("NFKD", S).encode("ASCII", "ignore")
    S = S.lower()
    return S

def maior_palindromo_dado_meio (S, i, tipo):
    # Se for impar o meio é composto por uma letra
    if tipo == "i":
        inicio = i
        fim = i
    elif tipo == "p":
        inicio = i
        fim = i+1

    # Se for impar o meio é composto por duas letras
    while (inicio - 1 >= 0 
            and fim + 1 < len(S) 
            and S[inicio] == S[fim]):
        inicio = inicio - 1
        fim = fim + 1
        while (S[inicio] == " "):
            inicio -= 1
        while(S[fim] == " "):
            fim += 1
    
    if(S[inicio] != S[fim]):
        inicio+=1
        fim-=1

    return (inicio, fim)
            
#Acha o maior palindromo a partir do caracter do meio
def maior_palindromo (S):
    fim_maior = 0
    inicio_maior = 0
    S = retira_uppercase_acentos(S)
    for i in range(1, len(S)-1):
        inicio, fim = maior_palindromo_dado_meio(S, i, "i")
        if(fim - inicio > fim_maior - inicio_maior):
            inicio_maior = inicio
            fim_maior = fim
    
    # Repetimos o código para o caso do palíndromo ser par
    for i in range(1, len(S)-1):
        inicio, fim = maior_palindromo_dado_meio(S, i, "p")
        if(fim - inicio > fim_maior - inicio_maior):
            inicio_maior = inicio
            fim_maior = fim
    
    S = S[inicio_maior:fim_maior+1]
    S = retira_espacos(S)
    return(S)