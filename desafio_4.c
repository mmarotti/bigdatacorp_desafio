#include <stdio.h>
#include <stdlib.h>

typedef struct aux{
    int chave;
    struct aux *esq, *dir;
} No;

No* criaNo (int chave){
    No *novo = (No*) malloc(sizeof(No));
    novo->chave = chave;
    novo->esq = NULL;
    novo->dir = NULL;
    return novo;
}

No* insere(No* raiz, No *novo){
    if (raiz==NULL) return novo;
    if (novo->chave <= raiz->chave) raiz->esq = insere(raiz->esq, novo);
    else raiz->dir = insere(raiz->dir, novo); 
    return raiz;   
}

int altura(No* no) { 
    if (no==NULL) return 0; 
    else { 
        /* Calcula a altura de cada subàrvore */
        int altura_esq = altura(no->esq); 
        int altura_dir = altura(no->dir); 
  
        /* Retorna a maior */
        if (altura_esq > altura_dir) return(altura_esq+1); 
        else return(altura_dir+1); 
    } 
}

/* Printa os nós de um certo nível*/
void imprime_dado_nivel(No* raiz, int level) { 
    if (raiz == NULL) return; 
    if (level == 1) printf("%d ", raiz->chave); 
    else if (level > 1) { 
        imprime_dado_nivel(raiz->esq, level-1); 
        imprime_dado_nivel(raiz->dir, level-1); 
    } 
} 

void imprime_em_largura(No* raiz) { 
    int h = altura(raiz); 
    int i; 
    for (i=1; i<=h; i++) imprime_dado_nivel(raiz, i); 
} 

No* buscaPai(No* raiz, int chave, No** pai){
    if (raiz->chave == chave) return raiz;
    if (chave <= raiz->chave){
        *pai = raiz;
        buscaPai(raiz->esq, chave, pai);     
    } else {
        *pai = raiz;
        buscaPai(raiz->dir, chave, pai); 
    }
}

No* removeNo(No* raiz, int chave){
    No *no, *pai, *x, *paiX;
    no = buscaPai(raiz, chave, &pai);
    if (!no->esq || !no->dir){
        if (!no->esq) x = no->dir;
        else x = no->esq;
    } else {
        x = no->esq;
        while (x->dir != NULL){
            paiX = x;
            x = x->dir;
        }
        if (paiX != no){
            paiX->dir = x->esq;
            x->esq = no->esq;
        }
        x->dir = no->dir;
    }
    if(!pai){
        free(no);
        return x;
    }
    if(chave < raiz->chave) pai->esq = x;
    else pai->dir = x;
    free(no);
    return raiz;   
}

int main(){
    No *raiz = criaNo(12);
    No *teste1 = criaNo(10);
    No *teste2 = criaNo(15);
    No *teste3 = criaNo(9);
    No *teste4 = criaNo(11);
    No *teste5 = criaNo(13);
    No *teste6 = criaNo(14);
    No *teste7 = criaNo(8);    
    insere(raiz, teste1);
    insere(raiz, teste2);
    insere(raiz, teste3);
    insere(raiz, teste4);
    insere(raiz, teste5);
    insere(raiz, teste6);
    insere(raiz, teste7);
    imprime_em_largura(raiz);
    printf("\n");
}
