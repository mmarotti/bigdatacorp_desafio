# -*- coding: utf-8 -*-


def indica_soma(a, s):
    resultado = [
        [s-valor, valor, ]
        for i, valor in enumerate(a)
        if s-valor in a[i+1:]
    ]
 
    if resultado:
        return True, resultado
    else:
        return False, resultado