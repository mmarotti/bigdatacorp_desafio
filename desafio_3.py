# -*- coding: utf-8 -*-


def melhor_posicionamento():
    A1 = int(input("Insira o número de pessoas no primeiro andar: "))
    A2 = int(input("Insira o número de pessoas no segundo andar: "))
    A3 = int(input("Insira o número de pessoas no terceiro andar: "))

    if (A1 >= 0 and A3 <= 1000):
        tempo_por_andar = [
            A1*(abs(0 - andar)) + A2*(abs(1 - andar)) + A3*(abs(2 - andar)) 
            for andar in range (0,3)
        ]
        return min(tempo_por_andar)
    else:
        raise Exception('Valores de entrada inválidos')